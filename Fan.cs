﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlchemyFanController2
{
    class Fan
    {
        public string name;
        public int index;
        public CurveSettings curveSettings = new CurveSettings();
        public double refreshInterval = 2000;
        public Fan(string name, int index, double refreshInterval)
        {
            this.name = name;
            this.index = index;
            this.refreshInterval = refreshInterval;
        }
        internal class CurveSettings
        {
            public int coreTempBasis = 0;
            public List<GraphControlPoint> graphControlPoints = new List<GraphControlPoint>();
            public double speedPos = 0;
            public double tempPos = 0;
            public double speedRangeMin = 0;
            public double speedRangeMax = 100;
            public double tempRangeMin = 0;
            public double tempRangeMax = 100;
            public bool staticFanSpeedEnabled = true;
            public double staticFanSpeed = 50;
            public CustomLine linearLinePlaceHolder;

            public CurveSettings(int coreTempBasis, double speedPos, double tempPos, double speedRangeMin, double speedRangeMax, double tempRangeMin, double tempRangeMax, bool staticFanSpeedEnabled, double staticFanSpeed)
            {
                this.coreTempBasis = coreTempBasis;
                this.speedPos = speedPos;
                this.tempPos = tempPos;
                this.speedRangeMin = speedRangeMin;
                this.speedRangeMax = speedRangeMax;
                this.tempRangeMin = tempRangeMin;
                this.tempRangeMax = tempRangeMax;
                this.staticFanSpeedEnabled = staticFanSpeedEnabled;
                this.staticFanSpeed = staticFanSpeed;
            }

            public CurveSettings()
            {
                
            }
            public enum CoreTempBasis
            {
                average = 0,
                hottest = 1
            }

        }
    }


}
