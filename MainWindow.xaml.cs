﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Brushes = System.Drawing.Brushes;
using Point = System.Windows.Point;
using SystemFonts = System.Drawing.SystemFonts;

namespace AlchemyFanController2
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private SpeedFanInterface speedFanInterface = new SpeedFanInterface();
        private List<RadioButton> fanRadioButtons = new List<RadioButton>();
        
        private Graphics graphGraphics;
        private const double graphstartX = 0;
        private const double graphstartY = 0;
        private double graphWidth;
        private double graphHeight;
        private int speedDivCount = 10;
        private int tempDivCount = 10;
        private List<Label> graphDivLabels = new List<Label>();
        private List<CustomLine> graphDivLines = new List<CustomLine>();
        private List<CustomLine> orderedLines = new List<CustomLine>();

        private List<Profile> profiles = new List<Profile>();
        private int selectedProfile;
        private int selectedGraphPoint = -1;
        private int lastgraphPointIndex;
        


        private Timer updateTimer;

        public MainWindow()
        {
            InitializeComponent();
            loadSettings();
            initFanSettings();
        }

        private void testfunc(object sender, RoutedEventArgs e)
        {
            speedFanInterface.updateCoreTemps();
            speedFanInterface.setFanSpeed(profiles[selectedProfile].fanIndex, temperatureToSpeed(speedFanInterface.temperatures[profiles[selectedProfile].fanIndex], profiles[selectedProfile].fanIndex));
        }

        public int temperatureToSpeed(int temperature, int fanIndex)
        {
            //if static fan speed is on, then return that
            if (profiles[selectedProfile].fans[fanIndex].curveSettings.staticFanSpeedEnabled)
                return (int) profiles[selectedProfile].fans[fanIndex].curveSettings.staticFanSpeed;

            //count amount of graph points, if 0 then evaluate over the linear line
            if (profiles[selectedProfile].fans[fanIndex].curveSettings.graphControlPoints.Count < 1)
                return (int) Math.Round((temperature - profiles[selectedProfile].fans[fanIndex].curveSettings.tempRangeMin) * (profiles[selectedProfile].fans[fanIndex].curveSettings.speedRangeMax -
                    profiles[selectedProfile].fans[fanIndex].curveSettings.speedRangeMin) / (profiles[selectedProfile].fans[fanIndex].curveSettings.tempRangeMax - profiles[selectedProfile].fans[fanIndex].curveSettings.tempRangeMin) +
                    profiles[selectedProfile].fans[fanIndex].curveSettings.speedRangeMin);

            GraphControlPoint[] orderedGraphControlPoints = profiles[selectedProfile].fans[fanIndex].curveSettings.graphControlPoints.ToArray();
            orderedGraphControlPoints = orderedGraphControlPoints.OrderBy(x => x.x).ToArray();
            int selectedLineIndex = 0;
            //i = 1 to skip the leftmost line which will be null
            for (int i = 1; i < orderedGraphControlPoints.Length; i++)
                //if the temperature is within the bounds of a line, then thats the line i want
                if (temperature > normalizeGraphPointX(orderedGraphControlPoints[i].leftLine.x2) && temperature < normalizeGraphPointX(orderedGraphControlPoints[i].leftLine.x1))
                    selectedLineIndex = i;


            double nX1 = normalizeGraphPointX(orderedGraphControlPoints[selectedLineIndex].leftLine.x2);
            double nX2 = normalizeGraphPointX(orderedGraphControlPoints[selectedLineIndex].leftLine.x1);
            double nY1 = normalizeGraphPointY(orderedGraphControlPoints[selectedLineIndex].leftLine.y1);
            double nY2 = normalizeGraphPointY(orderedGraphControlPoints[selectedLineIndex].leftLine.y2);
            return (int) Math.Round((temperature - nX1) * (nY2 - nY1) / (nX2 - nX1) + nY1);
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            //this needs to be called because the curve method wont work without a fully loaded window
            updateControlPointCurve();
        }

        private void button_createProfile_Click(object sender, RoutedEventArgs e)
        {
            AddProfile profileForm = new AddProfile();
            profileForm.ShowDialog();
            //actually add the profile now
        }

        private void button_deleteProfile_Click(object sender, RoutedEventArgs e)
        {
        }

        private void button_modifyProfile_Click(object sender, RoutedEventArgs e)
        {
        }

        private void comboBox_fanProfiles_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }

        #region fan initialization

        public void initFanSettings()
        {
            graphWidth = canvas_graph.Width;
            graphHeight = canvas_graph.Height;
            if (profiles.Count < 1)
                profiles.Add(new Profile(0, new Fan[0], "Default"));
            Fan[] tempFanArr = profiles[selectedProfile].fans.ToArray();
            profiles[selectedProfile].fans = new Fan[speedFanInterface.fanValuePatterns.Length];
            //sort through the fans and find any without settings. if there are, then fill the gaps
            for (int i = 0; i < speedFanInterface.fanValuePatterns.Length; i++)
            {
                bool found = false;
                for (int j = 0; j < tempFanArr.Length; j++)
                    if (tempFanArr[j].index == i)
                    {
                        profiles[selectedProfile].fans[i] = tempFanArr[j];
                        found = true;
                    }
                if (!found)
                    profiles[selectedProfile].fans[i] = new Fan("Unassigned", i, 2000);
            }

            for (int i = 0; i < profiles[selectedProfile].fans.Length; i++)
            {
                //we need to initialize the graph control points so they know where to be relative to the canvas.
                //so call addgraphpoint for all of them
                List<GraphControlPoint> drainControlPoints = profiles[selectedProfile].fans[i].curveSettings.graphControlPoints;
                profiles[selectedProfile].fans[i].curveSettings.graphControlPoints = new List<GraphControlPoint>();
                for (int j = 0; j < drainControlPoints.Count; j++)
                    //its fine to reference x and y here instead of Canvas.Get(etc) because there havent been any changes yet
                    addgraphPoint(drainControlPoints[j].x, drainControlPoints[j].y);
                //because of that ^, we need to reset the selected graph point to -1
                selectedGraphPoint = -1;

                //set up radio buttons
                fanRadioButtons.Add(new RadioButton());
                fanRadioButtons[i].IsChecked = false;
                fanRadioButtons[i].Checked += fanRadioButtonChecked;
                fanRadioButtons[i].Content = profiles[selectedProfile].fans[i].name + " " + i;
                canvas_fanList.Children.Add(fanRadioButtons[i]);
                Canvas.SetLeft(fanRadioButtons[i], 12);
                Canvas.SetTop(fanRadioButtons[i], 20 + 30 * i);
            }
            fanRadioButtons[0].IsChecked = true;
        }

        public void fanRadioButtonChecked(object sender, EventArgs e)
        {
            for (int i = 0; i < fanRadioButtons.Count; i++)
                if ((bool) fanRadioButtons[i].IsChecked)
                    profiles[selectedProfile].fanIndex = i;
            textbox_fanName.Text = profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].name;
            integerUpDown_refreshInterval.Value = (int?) profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].refreshInterval;
            integerUpDown_speedPos.Value = (int?) profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.speedPos;
            integerUpDown_tempPos.Value = (int?) profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.tempPos;
            integerUpDown_speedRangeMin.Value = (int?) profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.speedRangeMin;
            integerUpDown_speedRangeMax.Value = (int?) profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.speedRangeMax;
            integerUpDown_tempRangeMin.Value = (int?) profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.tempRangeMin;
            integerUpDown_tempRangeMax.Value = (int?) profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.tempRangeMax;
            integerUpDown_staticFanSpeed.Value = (int?) profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.staticFanSpeed;
            integerUpDown_refreshInterval.Value = (int?) profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].refreshInterval;
            checkBox_staticFanSpeed.IsChecked = profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.staticFanSpeedEnabled;
            if (profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.coreTempBasis == 0)
                checkBox_averageCores.IsChecked = true;
            else checkBox_hottestCore.IsChecked = false;
            //build graph
            buildGraph();
        }

        #endregion


        #region Save/Load Settings

        private void saveSettings()
        {
            for (int i = 0; i < profiles.Count; i++)
            {
                StringBuilder profileText = new StringBuilder();
                profileText.Append("profileName:" + profiles[i].name + "\n");
                profileText.Append("fanCount:" + profiles[i].fans.Length + "\n");
                for (int j = 0; j < profiles[i].fans.Length; j++)
                {
                    profileText.Append("fanName:" + profiles[i].fans[j].name + "\n");
                    profileText.Append("fanIndex:" + profiles[i].fans[j].index + "\n");
                    profileText.Append("fanRefresh:" + profiles[i].fans[j].refreshInterval + "\n");
                    profileText.Append("fanCurve;coreTempBasis:" + profiles[i].fans[j].curveSettings.coreTempBasis + "\n");
                    profileText.Append("fanCurve;speedPos:" + profiles[i].fans[j].curveSettings.speedPos + "\n");
                    profileText.Append("fanCurve;tempPos:" + profiles[i].fans[j].curveSettings.tempPos + "\n");
                    profileText.Append("fanCurve;speedRangeMin:" + profiles[i].fans[j].curveSettings.speedRangeMin + "\n");
                    profileText.Append("fanCurve;speedRangeMax:" + profiles[i].fans[j].curveSettings.speedRangeMax + "\n");
                    profileText.Append("fanCurve;tempRangeMin:" + profiles[i].fans[j].curveSettings.tempRangeMin + "\n");
                    profileText.Append("fanCurve;tempRangeMax:" + profiles[i].fans[j].curveSettings.tempRangeMax + "\n");
                    profileText.Append("fanCurve;staticFanSpeedEnabled:" + profiles[i].fans[j].curveSettings.staticFanSpeedEnabled + "\n");
                    profileText.Append("fanCurve;staticFanSpeed:" + profiles[i].fans[j].curveSettings.staticFanSpeed + "\n");
                    for (int k = 0; k < profiles[i].fans[j].curveSettings.graphControlPoints.Count; k++)
                        profileText.Append("graphControlPoint:" + Canvas.GetLeft(profiles[i].fans[j].curveSettings.graphControlPoints[k]) + "." + Canvas.GetRight(profiles[i].fans[j].curveSettings.graphControlPoints[k]) + "\n");
                }
                File.WriteAllText(profiles[i].name + ".prof", profileText.ToString());
            }
        }

        private void loadSettings()
        {
            string[] files = Directory.GetFiles(Environment.CurrentDirectory, "*.prof");
            List<Profile> tempProfiles = new List<Profile>();
            for (int i = 0; i < files.Length; i++)
            {
                Profile tempProfile = new Profile();
                string[] inputLines = File.ReadAllText(files[i]).Split(new[] {"\n"}, StringSplitOptions.None);
                for (int j = 0; j < inputLines.Length; j++)
                    if (inputLines[j] != "" && !inputLines[j].StartsWith("graphControlPoint"))
                        inputLines[j] = inputLines[j].Split(':')[1];
                int inputCounter = 0;
                tempProfile.name = inputLines[inputCounter++];
                tempProfile.fans = new Fan[int.Parse(inputLines[inputCounter++])];
                for (int j = 0; j < tempProfile.fans.Length; j++)
                {
                    tempProfile.fans[j] = new Fan("", 0, 0)
                    {
                        name = inputLines[inputCounter++],
                        index = int.Parse(inputLines[inputCounter++]),
                        refreshInterval = Convert.ToDouble(inputLines[inputCounter++]),
                        curveSettings =
                        {
                            coreTempBasis = Convert.ToInt32(inputLines[inputCounter++]),
                            speedPos = Convert.ToDouble(inputLines[inputCounter++]),
                            tempPos = Convert.ToDouble(inputLines[inputCounter++]),
                            speedRangeMin = Convert.ToDouble(inputLines[inputCounter++]),
                            speedRangeMax = Convert.ToDouble(inputLines[inputCounter++]),
                            tempRangeMin = Convert.ToDouble(inputLines[inputCounter++]),
                            tempRangeMax = Convert.ToDouble(inputLines[inputCounter++]),
                            staticFanSpeedEnabled = Convert.ToBoolean(inputLines[inputCounter++]),
                            staticFanSpeed = Convert.ToDouble(inputLines[inputCounter++])
                        }
                    };
                    while (inputLines[inputCounter].StartsWith("graphControlPoint"))
                    {
                        string parsedLine = inputLines[inputCounter].Split(':')[1];
                        tempProfile.fans[j].curveSettings.graphControlPoints.Add(new GraphControlPoint(Convert.ToDouble(parsedLine.Split('.')[0]), Convert.ToDouble(parsedLine.Split('.')[1])));
                        inputCounter++;
                    }
                }
                tempProfiles.Add(tempProfile);
            }
            profiles = tempProfiles;
        }

        #endregion


        #region Normalize methods

        private double normalizeGraphPointX(double pixelLocation)
        {
            double tempRangeMax = profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.tempRangeMax;
            double tempRangeMin = profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.tempRangeMin;
            return Clamp((pixelLocation - graphstartX) * (tempRangeMax - tempRangeMin) / graphWidth + tempRangeMin, tempRangeMin, tempRangeMax);
        }

        private double normalizeGraphPointY(double pixelLocation)
        {
            double speedRangeMin = profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.speedRangeMin;
            double speedRangeMax = profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.speedRangeMax;
            return Clamp(speedRangeMax - speedRangeMin - (pixelLocation - graphstartY) * (speedRangeMax - speedRangeMin) / graphHeight + speedRangeMin, speedRangeMin, speedRangeMax);
        }

        private double deNormalizeGraphPointX(double normalLocation)
        {
            double tempRangeMax = profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.tempRangeMax;
            double tempRangeMin = profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.tempRangeMin;
            return (graphstartX * tempRangeMax - graphstartX * tempRangeMin - tempRangeMin * graphWidth + normalLocation * graphWidth) / (tempRangeMax - tempRangeMin);
        }

        private double deNormalizeGraphPointY(double normalLocation)
        {
            double speedRangeMin = profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.speedRangeMin;
            double speedRangeMax = profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.speedRangeMax;
            return (speedRangeMax * (graphstartY + graphHeight) - speedRangeMin * graphstartY - graphHeight * normalLocation) / (speedRangeMax - speedRangeMin);
        }

        public double Clamp(double val, double min, double max)
        {
            if (val.CompareTo(min) < 0) return min;
            return val.CompareTo(max) > 0 ? max : val;
        }

        #endregion


        #region Graph construction

        private void buildGraph()
        {
            canvas_graph.Children.RemoveRange(0, canvas_graph.Children.Count);
            for (int i = 0; i < profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints.Count; i++)
                canvas_graph.Children.Add(profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[i]);
            updateControlPointCurve();
            if (profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints.Count > 0)
            {
                integerUpDown_tempPos.Value = (int?) normalizeGraphPointX(Canvas.GetLeft(profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[0]));
                integerUpDown_speedPos.Value = (int?) normalizeGraphPointY(Canvas.GetTop(profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[0]));
            }
            graphDivLines.Clear();
            graphDivLabels.Clear();
            graphGraphics = Graphics.FromHwnd(IntPtr.Zero);
            for (int i = 0; i < speedDivCount + 1; i++)
            {
                graphDivLines.Add(new CustomLine(graphstartX, graphstartY + graphHeight / speedDivCount * i, graphstartX + graphWidth, graphstartY + graphHeight / speedDivCount * i));
                graphDivLabels.Add(new Label());

                graphDivLabels[graphDivLabels.Count - 1].Content = profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.speedRangeMin +
                                                                   (speedDivCount - i) * ((profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.speedRangeMax - profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.speedRangeMin) / 10.0) + "%";
                SizeF stringMeasure = graphGraphics.MeasureString(graphDivLabels[graphDivLabels.Count - 1].Content.ToString(), SystemFonts.DefaultFont);
                Canvas.SetLeft(graphDivLabels[graphDivLabels.Count - 1], graphstartX - stringMeasure.Width * 1.1);
                Canvas.SetTop(graphDivLabels[graphDivLabels.Count - 1], graphstartY + graphHeight / speedDivCount * i - stringMeasure.Height);

                canvas_graph.Children.Add(graphDivLines[graphDivLines.Count - 1]);
                canvas_graph.Children.Add(graphDivLabels[graphDivLabels.Count - 1]);
            }
            for (int i = 0; i < tempDivCount + 1; i++)
            {
                graphDivLines.Add(new CustomLine(graphstartX + graphWidth / tempDivCount * i, graphstartY, graphstartX + graphWidth / tempDivCount * i, graphstartY + graphHeight));
                graphDivLabels.Add(new Label());

                graphDivLabels[graphDivLabels.Count - 1].Content = profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.tempRangeMin +
                                                                   i * ((profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.tempRangeMax - profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.tempRangeMin) / 10.0) + "C";
                SizeF stringMeasure = graphGraphics.MeasureString(graphDivLabels[graphDivLabels.Count - 1].Content.ToString(), SystemFonts.DefaultFont);
                Canvas.SetLeft(graphDivLabels[graphDivLabels.Count - 1], graphstartX + graphWidth / tempDivCount * i - stringMeasure.Width / 2f);
                Canvas.SetTop(graphDivLabels[graphDivLabels.Count - 1], graphstartY + graphHeight);

                canvas_graph.Children.Add(graphDivLines[graphDivLines.Count - 1]);
                canvas_graph.Children.Add(graphDivLabels[graphDivLabels.Count - 1]);
            }
            Bitmap backMap = new Bitmap((int) (graphstartX + graphWidth), (int) (graphstartY + graphHeight));
            Graphics backGraphics = Graphics.FromImage(backMap);
            backGraphics.FillRectangle(Brushes.LightGray, new RectangleF((int) graphstartX, (int) graphstartY, (int) (graphWidth + graphstartX), (int) (graphHeight + graphstartY)));
            backMap.Save("temp.png");
            BitmapImage backMapImage = new BitmapImage();
            backMapImage.BeginInit();
            backMapImage.CacheOption = BitmapCacheOption.OnLoad;
            backMapImage.UriSource = new Uri(AppDomain.CurrentDomain.BaseDirectory + "\\temp.png");
            canvas_graph.Background = new ImageBrush(backMapImage);
            backMapImage.EndInit();
            File.Delete("temp.png");
        }

        private void buildGraphLabelsOnly()
        {
            foreach (Label v in graphDivLabels)
                canvas_graph.Children.Remove(v);
            graphDivLabels.Clear();
            for (int i = 0; i < speedDivCount + 1; i++)
            {
                graphDivLabels.Add(new Label());

                graphDivLabels[graphDivLabels.Count - 1].Content = profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.speedRangeMin +
                                                                   (speedDivCount - i) * ((profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.speedRangeMax - profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.speedRangeMin) / 10.0) + "%";
                SizeF stringMeasure = graphGraphics.MeasureString(graphDivLabels[graphDivLabels.Count - 1].Content.ToString(), SystemFonts.DefaultFont);
                Canvas.SetLeft(graphDivLabels[graphDivLabels.Count - 1], graphstartX - stringMeasure.Width * 1.1);
                Canvas.SetTop(graphDivLabels[graphDivLabels.Count - 1], graphstartY + graphHeight / speedDivCount * i - stringMeasure.Height);
                canvas_graph.Children.Add(graphDivLabels[graphDivLabels.Count - 1]);
            }
            for (int i = 0; i < tempDivCount + 1; i++)
            {
                graphDivLabels.Add(new Label());
                graphDivLabels[graphDivLabels.Count - 1].Content = profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.tempRangeMin +
                                                                   i * ((profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.tempRangeMax - profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.tempRangeMin) / 10.0) + "C";
                SizeF stringMeasure = graphGraphics.MeasureString(graphDivLabels[graphDivLabels.Count - 1].Content.ToString(), SystemFonts.DefaultFont);
                Canvas.SetLeft(graphDivLabels[graphDivLabels.Count - 1], graphstartX + graphWidth / tempDivCount * i - stringMeasure.Width / 2f);
                Canvas.SetTop(graphDivLabels[graphDivLabels.Count - 1], graphstartY + graphHeight);
                canvas_graph.Children.Add(graphDivLabels[graphDivLabels.Count - 1]);
            }
        }

        #endregion


        #region add to / update graph

        private void addgraphPoint(double X, double Y)
        {
            foreach (GraphControlPoint control in profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints)
            {
                Point tempPoint = control.TransformToAncestor(canvas_graph).Transform(new Point(0, 0));
                if (Math.Abs(tempPoint.X - X) < 1 && Math.Abs(tempPoint.Y - Y) < 1)
                    return;
            }
            profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints.Add(new GraphControlPoint(X, Y));
            canvas_graph.Children.Add(profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints.Count - 1]);
            Canvas.SetLeft(profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints.Count - 1], X);
            Canvas.SetTop(profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints.Count - 1], Y);
            Panel.SetZIndex(profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints.Count - 1], 10);
            lastgraphPointIndex = profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints.Count - 1;
            integerUpDown_tempPos.Value = (int) normalizeGraphPointX(Canvas.GetLeft(profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints.Count - 1]));
            integerUpDown_speedPos.Value = (int) normalizeGraphPointY(Canvas.GetTop(profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints.Count - 1]));
            updateControlPointCurve();
            saveSettings();
        }

        private void updateControlPointCurve()
        {
            List<GraphControlPoint> graphControlPoints = profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints;
            if (graphControlPoints.Count == 0)
            {
                profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.linearLinePlaceHolder = new CustomLine(deNormalizeGraphPointX(0), deNormalizeGraphPointY(0) - 4, deNormalizeGraphPointX(profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.tempRangeMax),
                    deNormalizeGraphPointY(profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.speedRangeMax));
                canvas_graph.Children.Add(profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.linearLinePlaceHolder);
                return;
            }
            for (int sourceControlIndexRef = 0; sourceControlIndexRef < graphControlPoints.Count; sourceControlIndexRef++)
            {
                List<GraphControlPoint> orderedGraphControlPoints = graphControlPoints.OrderBy(Canvas.GetLeft).ToList();
                int sourceControlIndex = orderedGraphControlPoints.IndexOf(graphControlPoints[sourceControlIndexRef]);
                int leftControlIndexRef = 0;
                if (sourceControlIndex > 0)
                    leftControlIndexRef = graphControlPoints.IndexOf(orderedGraphControlPoints[sourceControlIndex - 1]);
                int rightControlIndexRef = 0;
                if (sourceControlIndex < orderedGraphControlPoints.Count - 1)
                    rightControlIndexRef = graphControlPoints.IndexOf(orderedGraphControlPoints[sourceControlIndex + 1]);
                Point sourceControlPoint = new Point(Canvas.GetLeft(graphControlPoints[sourceControlIndexRef]), Canvas.GetTop(graphControlPoints[sourceControlIndexRef]));
                canvas_graph.Children.Remove(graphControlPoints[sourceControlIndexRef].leftLine);
                canvas_graph.Children.Remove(graphControlPoints[sourceControlIndexRef].rightLine);
                if (sourceControlIndex > 0)
                {
                    //left control exists
                    if (graphControlPoints[leftControlIndexRef].rightLine != null)
                    {
                        canvas_graph.Children.Remove(graphControlPoints[leftControlIndexRef].rightLine);
                        graphControlPoints[leftControlIndexRef].rightLine = null;
                    }
                    Point leftControlPoint = graphControlPoints[leftControlIndexRef].TransformToAncestor(canvas_graph).Transform(new Point(0, 0));
                    graphControlPoints[sourceControlIndexRef].leftLine = new CustomLine(sourceControlPoint.X, sourceControlPoint.Y, leftControlPoint.X, leftControlPoint.Y);
                    canvas_graph.Children.Add(graphControlPoints[sourceControlIndexRef].leftLine);
                }
                else
                {
                    //left control does not exist
                    graphControlPoints[sourceControlIndexRef].leftLine = new CustomLine(sourceControlPoint.X, sourceControlPoint.Y, graphstartX, graphstartY + graphHeight);
                    canvas_graph.Children.Add(graphControlPoints[sourceControlIndexRef].leftLine);
                }
                if (sourceControlIndex < orderedGraphControlPoints.Count - 1)
                {
                    //right control exists
                    if (graphControlPoints[rightControlIndexRef].leftLine != null)
                    {
                        canvas_graph.Children.Remove(graphControlPoints[rightControlIndexRef].leftLine);
                        graphControlPoints[rightControlIndexRef].leftLine = null;
                    }
                    Point rightControlPoint = graphControlPoints[rightControlIndexRef].TransformToAncestor(canvas_graph).Transform(new Point(0, 0));
                    graphControlPoints[sourceControlIndexRef].rightLine = new CustomLine(sourceControlPoint.X, sourceControlPoint.Y, rightControlPoint.X, rightControlPoint.Y);
                    canvas_graph.Children.Add(graphControlPoints[sourceControlIndexRef].rightLine);
                }
                else
                {
                    //right control does not exist
                    graphControlPoints[sourceControlIndexRef].rightLine = new CustomLine(sourceControlPoint.X, sourceControlPoint.Y, graphstartX + graphWidth, graphstartY);
                    canvas_graph.Children.Add(graphControlPoints[sourceControlIndexRef].rightLine);
                }
                canvas_graph.Children.Remove(profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.linearLinePlaceHolder);
            }
            List<GraphControlPoint> orderedGraphControlPointsUpdated = graphControlPoints.OrderBy(Canvas.GetLeft).ToList();
            orderedLines.Clear();
            for (int i = 0; i < orderedGraphControlPointsUpdated.Count; i++)
            {
                if (orderedGraphControlPointsUpdated[i].leftLine != null)
                    orderedLines.Add(orderedGraphControlPointsUpdated[i].leftLine);
                if (orderedGraphControlPointsUpdated[i].rightLine != null)
                    orderedLines.Add(orderedGraphControlPointsUpdated[i].rightLine);
            }
            saveSettings();
        }

        #endregion


        #region window events

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount > 1 && e.RightButton != MouseButtonState.Pressed)
            {
                //isMouseDown = true;
                addgraphPoint(e.GetPosition(canvas_graph).X, e.GetPosition(canvas_graph).Y);
                return;
            }
            for (int index = 0; index < profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints.Count; index++)
            {
                double i1 = e.GetPosition(profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[index]).X - profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[index].Width / 2;
                double i2 = e.GetPosition(profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[index]).Y - profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[index].Height / 2;
                if (i1 < profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[index].Width / 2 && i1 > profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[index].Width / 2 * -1 &&
                    i2 < profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[index].Height / 2 && i2 > profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[index].Height / 2 * -1)
                {
                    lastgraphPointIndex = index;
                    if (e.RightButton == MouseButtonState.Pressed && lastgraphPointIndex != -1)
                    {
                        selectedGraphPoint = -1;
                        if (profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[index].leftLine != null)
                            canvas_graph.Children.Remove(profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[index].leftLine);
                        if (profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[index].rightLine != null)
                            canvas_graph.Children.Remove(profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[index].rightLine);
                        canvas_graph.Children.Remove(profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[index]);
                        profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints.RemoveAt(index);
                        integerUpDown_tempPos.Value = 0;
                        integerUpDown_speedPos.Value = 0;
                        lastgraphPointIndex = -1;
                        updateControlPointCurve();
                        saveSettings();
                        return;
                    }
                    integerUpDown_tempPos.Value = (int?) normalizeGraphPointX(Canvas.GetLeft(profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[index]));
                    integerUpDown_speedPos.Value = (int?) normalizeGraphPointY(Canvas.GetTop(profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[index]));
                    selectedGraphPoint = index;

                    saveSettings();
                }
            }
        }

        private void Window_MouseUp(object sender, MouseButtonEventArgs e)
        {
            selectedGraphPoint = -1;
            //graphPointLockedOn = false;
        }

        private void Window_MouseMove(object sender, MouseEventArgs e)
        {

            if (selectedGraphPoint < 0) return;
            double outerLimitseX = e.GetPosition(canvas_graph).X;
            double outerLimitseY = e.GetPosition(canvas_graph).Y;
            if (outerLimitseX >= graphstartX + profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[selectedGraphPoint].Width / 2.0 &&
                outerLimitseX <= graphstartX + graphWidth + profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[selectedGraphPoint].Width / 2.0)
            {
                //graphPointLockedOn = true;
                Canvas.SetLeft(profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[selectedGraphPoint],
                    e.GetPosition(profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[selectedGraphPoint]).X + Canvas.GetLeft(profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[selectedGraphPoint]) -
                    profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[selectedGraphPoint].Width / 2.0);
                updateControlPointCurve();
                integerUpDown_tempPos.Value = (int?) normalizeGraphPointX(Canvas.GetLeft(profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[selectedGraphPoint]));
            }
            if (outerLimitseY >= graphstartY + profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[selectedGraphPoint].Height / 2.0 &&
                outerLimitseY <= graphstartY + graphHeight + profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[selectedGraphPoint].Height / 2.0)
            {
                //graphPointLockedOn = true;
                Canvas.SetTop(profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[selectedGraphPoint],
                    e.GetPosition(profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[selectedGraphPoint]).Y + Canvas.GetTop(profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[selectedGraphPoint]) -
                    profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[selectedGraphPoint].Height / 2.0);
                updateControlPointCurve();
                integerUpDown_speedPos.Value = (int?) normalizeGraphPointY(Canvas.GetTop(profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[selectedGraphPoint]));
            }
        }

        #endregion


        #region input box events

        private void integerUpDown_tempPos_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints.Count < 1)
                return;
            //have to have everything here to prevent glitches
            if (e.Delta > 0 && integerUpDown_tempPos.Value < integerUpDown_tempPos.Maximum && integerUpDown_tempPos.Value < profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.tempRangeMax)
            {
                integerUpDown_tempPos.Value += 1;

            }
            if (e.Delta < 0 && integerUpDown_tempPos.Value > integerUpDown_tempPos.Minimum && integerUpDown_tempPos.Value > profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.tempRangeMin)
            {
                integerUpDown_tempPos.Value -= 1;

            }
            Canvas.SetLeft(profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[lastgraphPointIndex], deNormalizeGraphPointX((int) integerUpDown_tempPos.Value));
            profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.tempPos = (double) integerUpDown_tempPos.Value;
            updateControlPointCurve();
            saveSettings();
        }

        private void integerUpDown_speedPos_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints.Count < 1)
                return;
            //have to have everything here to prevent glitches
            if (e.Delta > 0 && integerUpDown_speedPos.Value < integerUpDown_speedPos.Maximum && integerUpDown_speedPos.Value < profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.speedRangeMax)
            {
                integerUpDown_speedPos.Value += 2;
                integerUpDown_speedPos.Value -= 1;
            }
            if (e.Delta < 0 && integerUpDown_speedPos.Value > integerUpDown_speedPos.Minimum && integerUpDown_speedPos.Value > profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.speedRangeMin)
            {
                integerUpDown_speedPos.Value -= 2;
                integerUpDown_speedPos.Value += 1;
            }
            profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.speedPos = (double) integerUpDown_speedPos.Value;
            Canvas.SetTop(profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[lastgraphPointIndex], deNormalizeGraphPointY((int) integerUpDown_speedPos.Value));
            updateControlPointCurve();
            saveSettings();
        }

        private void integerUpDown_speedRangeMin_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            //have to have everything here to prevent glitches
            if (e.Delta > 0 && profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.speedRangeMin < integerUpDown_speedRangeMin.Maximum)
                profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.speedRangeMin++;
            else if (e.Delta < 0 && profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.speedRangeMin > integerUpDown_speedRangeMin.Minimum)
                profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.speedRangeMin--;
            integerUpDown_speedRangeMin.Value = (int?) profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.speedRangeMin;
            buildGraphLabelsOnly();
            saveSettings();
            if (profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints.Count < 1)
                return;
            integerUpDown_speedPos.Value = (int?) normalizeGraphPointY(Canvas.GetTop(profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[lastgraphPointIndex]));
            profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.speedPos = (double) integerUpDown_speedPos.Value;
            updateControlPointCurve();
        }

        private void integerUpDown_speedRangeMax_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            //have to have everything here to prevent glitches
            if (e.Delta > 0 && profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.speedRangeMax < integerUpDown_speedRangeMax.Maximum)
                profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.speedRangeMax++;
            if (e.Delta < 0 && profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.speedRangeMax > integerUpDown_speedRangeMax.Minimum)
                profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.speedRangeMax--;
            integerUpDown_speedRangeMax.Value = (int?) profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.speedRangeMax;
            buildGraphLabelsOnly();
            saveSettings();
            if (profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints.Count < 1)
                return;
            integerUpDown_speedPos.Value = (int?) normalizeGraphPointY(Canvas.GetTop(profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[lastgraphPointIndex]));
            profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.speedPos = (double) integerUpDown_speedPos.Value;
            updateControlPointCurve();
        }

        private void integerUpDown_tempRangeMin_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            //have to have everything here to prevent glitches
            if (e.Delta > 0 && profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.tempRangeMin < integerUpDown_tempRangeMin.Maximum)
                profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.tempRangeMin++;
            else if (e.Delta < 0 && profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.tempRangeMin > integerUpDown_tempRangeMin.Minimum)
                profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.tempRangeMin--;
            integerUpDown_tempRangeMin.Value = (int?) profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.tempRangeMin;
            buildGraphLabelsOnly();
            saveSettings();
            if (profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints.Count < 1)
                return;
            integerUpDown_tempPos.Value = (int?) normalizeGraphPointX(Canvas.GetLeft(profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[lastgraphPointIndex]));
            profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.tempPos = (double) integerUpDown_tempPos.Value;
            updateControlPointCurve();
        }

        private void integerUpDown_tempRangeMax_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            //have to have everything here to prevent glitches
            if (e.Delta > 0 && profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.tempRangeMax < integerUpDown_tempRangeMax.Maximum)
                profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.tempRangeMax++;
            if (e.Delta < 0 && profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.tempRangeMax > integerUpDown_tempRangeMax.Minimum)
                profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.tempRangeMax--;
            integerUpDown_tempRangeMax.Value = (int?) profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.tempRangeMax;
            buildGraphLabelsOnly();
            saveSettings();
            if (profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints.Count < 1)
                return;
            integerUpDown_tempPos.Value = (int?) normalizeGraphPointX(Canvas.GetLeft(profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[lastgraphPointIndex]));
            profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.tempPos = (double) integerUpDown_tempPos.Value;
            updateControlPointCurve();
        }

        private void integerUpDown_staticFanSpeed_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            //have to have everything here to prevent glitches
            if (e.Delta > 0 && integerUpDown_staticFanSpeed.Value < integerUpDown_staticFanSpeed.Maximum)
                integerUpDown_staticFanSpeed.Value++;
            else if (e.Delta < 0 && integerUpDown_staticFanSpeed.Value > integerUpDown_staticFanSpeed.Minimum)
                integerUpDown_staticFanSpeed.Value--;
            profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.staticFanSpeed = (double) integerUpDown_staticFanSpeed.Value;
            saveSettings();
        }

        private void integerUpDown_speedPos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Enter) return;
            Canvas.SetTop(profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[lastgraphPointIndex], deNormalizeGraphPointY((int) integerUpDown_speedPos.Value));
            updateControlPointCurve();
            saveSettings();
        }

        private void integerUpDown_tempPos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Enter) return;
            Canvas.SetLeft(profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.graphControlPoints[lastgraphPointIndex], deNormalizeGraphPointX((int) integerUpDown_tempPos.Value));
            updateControlPointCurve();
            saveSettings();
        }

        private void textbox_fanName_KeyUp(object sender, KeyEventArgs e)
        {
            profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].name = textbox_fanName.Text;
            fanRadioButtons[profiles[selectedProfile].fanIndex].Content = profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].name;
            saveSettings();
        }

        private void integerUpDown_refreshInterval_KeyUp(object sender, KeyEventArgs e)
        {
            profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].refreshInterval = (double) integerUpDown_refreshInterval.Value;
            saveSettings();
        }

        private void checkBox_allowNegativeCurveSlope_Click(object sender, RoutedEventArgs e)
        {
            //fans[fanIndex].
            saveSettings();
        }

        private void checkBox_staticFanSpeed_Click(object sender, RoutedEventArgs e)
        {
            profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.staticFanSpeedEnabled = (bool) checkBox_staticFanSpeed.IsChecked;
            saveSettings();
        }

        private void checkBox_hottestCore_Checked(object sender, RoutedEventArgs e)
        {
            if ((bool) checkBox_hottestCore.IsChecked)
                profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.coreTempBasis = 0;
            else profiles[selectedProfile].fans[profiles[selectedProfile].fanIndex].curveSettings.coreTempBasis = 1;
            saveSettings();
        }

        #endregion
    }
}