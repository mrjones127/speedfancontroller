﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AlchemyFanController2
{
    /// <summary>
    /// Interaction logic for MyDialog.xaml
    /// </summary>
    public partial class AddProfile : Window
    {
        public AddProfile()
        {
            InitializeComponent();
        }

        public string ResponseText
        {
            get => textBox_Response.Text;
            set => textBox_Response.Text = value;
        }

        private void textBox_Response_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                Close();
        }
    }
}
