﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;

namespace AlchemyFanController2
{
    public class CustomLine : UserControl
    {
        public double x1, x2, y1, y2;
        public CustomLine(double x1, double y1, double x2, double y2)
        {
            this.x1 = x1;
            this.x2 = x2;
            this.y1 = y1;
            this.y2 = y2;
            Background = new SolidColorBrush(Colors.DarkGray);

            double rotationAngle = Math.Atan2(y2 - y1, x2 - x1) * 180 / Math.PI;
            RotateTransform angleTransform = new RotateTransform(rotationAngle);
            RenderTransform = angleTransform;
            Width = Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2));
            Height = 1;
            Canvas.SetLeft(this, x1);
            Canvas.SetTop(this, y1);
            Panel.SetZIndex(this, 0);
        }
    }
}
