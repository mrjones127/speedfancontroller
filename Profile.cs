﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlchemyFanController2
{
    class Profile
    {
        public int fanIndex;
        public Fan[] fans;
        public string name;
        public Profile(int fanIndex, Fan[] fans, string name)
        {
            this.fanIndex = fanIndex;
            this.fans = fans;
            this.name = name;
        }

        public Profile()
        {
            
        }
    }
}