﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Management;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using UIAutomationClient;

namespace AlchemyFanController2
{
    internal class SpeedFanInterface
    {
        //Definitions For Different Window Placement Constants
        //private const int SW_HIDE = 0;

        //private const int SW_SHOWNORMAL = 1;
        //private const int SW_NORMAL = 1;
        //private const int SW_SHOWMINIMIZED = 2;
        //private const int SW_SHOWMAXIMIZED = 3;
        //private const int SW_MAXIMIZE = 3;
        //private const int SW_SHOWNOACTIVATE = 4;
        //private const int SW_SHOW = 5;
        //private const int SW_MINIMIZE = 6;
        //private const int SW_SHOWMINNOACTIVE = 7;
        //private const int SW_SHOWNA = 8;
        //private const int SW_RESTORE = 9;

        public int[] fanSpeeds = new int[32];
        //private Process speedFanProcess;
        public IUIAutomationValuePattern[] fanValuePatterns = new IUIAutomationValuePattern[0];
        public int[] temperatures = new int[32];
        public int averageCpuTemp = 0;
        public int[] coreTemperatures; 
        public int updateInfoInterval = 1000;
        //private Task updateInfoTask;
        public float[] voltages = new float[32];

        public int coreCount;

        public SpeedFanInterface()
        {
            // foreach (Process p in Process.GetProcessesByName("speedfan"))
            //     p.Kill();
            // speedFanProcess = new Process {StartInfo = new ProcessStartInfo(@"C:\Program Files (x86)\SpeedFan\speedfan.exe", "/NOPCISCAN /NOSMBSCAN /NOSMARTSCAN /NOTOSHIBA /NOASUSSCAN /NONVIDIAI2C /NOAMDK8SCAN")};
            // speedFanProcess.Start();
            
            //TEMP DISABLE TO DEBUG -> ENABLE THIS TO BEGIN SPEEDFAN HOOKING
            getFanValuePatterns();
            foreach (ManagementBaseObject item in new ManagementObjectSearcher("Select * from Win32_Processor").Get())
                coreCount += int.Parse(item["NumberOfCores"].ToString());
        }

        [DllImport("SpeedfanInfo.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void updateInfo();

        [DllImport("SpeedfanInfo.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int getfan(int index);

        [DllImport("SpeedfanInfo.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int gettemperature(int index);

        [DllImport("SpeedfanInfo.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int getvoltage(int index);
        public void updateCoreTemps()
        {
            updateInfo();
            for (int i = 0; i < 32; i++)
                temperatures[i] = gettemperature(i);

            int index = 31;
            while (temperatures[index] == 0)
                index--;

            coreTemperatures = new int[coreCount];
            averageCpuTemp = 0;
            for (int i = coreCount - 1; i >= 0; i--)
            {
                coreTemperatures[i] = temperatures[index - coreCount];
                averageCpuTemp += temperatures[index - coreCount];
            }
            averageCpuTemp = averageCpuTemp / coreCount;
        }
        //public void updateFanInfo()
        //{
        //    for (int i = 0; i < 32; i++)
        //    {
        //        fanSpeeds[i] = getfan(i);
        //        temperatures[i] = gettemperature(i);
        //        voltages[i] = getvoltage(i);
        //    }
        //}

        public void setFanSpeed(int fanValuePatternIndex, int value)
        {
            fanValuePatterns[fanValuePatternIndex].SetValue(value.ToString());
        }

        public string getFanSpeed(int fanValuePatternIndex)
        {
            return fanValuePatterns[fanValuePatternIndex].CurrentValue;
        }

        public void setFanSpeedAll(int value)
        {
            for (int i = 0; i < fanValuePatterns.Length; i++)
                fanValuePatterns[i].SetValue(value + "");
        }

        public string[] getFanSpeedAll()
        {
            List<string> speeds = new List<string>();
            for (int i = 0; i < fanValuePatterns.Length; i++)
                speeds.Add(fanValuePatterns[i].CurrentValue);
            return speeds.ToArray();
        }

        [DllImport("user32.dll", EntryPoint = "FindWindow")]
        private static extern int FindWindow(string sClass, string sWindow);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool ShowWindow(IntPtr hWnd, ShowWindowEnum flags);

        [DllImport("user32.dll")]
        private static extern int SendMessage(IntPtr hWnd, int wMsg, IntPtr wParam, IntPtr lParam);

        [DllImport("user32.dll")]
        private static extern bool GetWindowRect(IntPtr hwnd, ref Rect rectangle);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern bool MoveWindow(IntPtr hWnd, int X, int Y, int nWidth, int nHeight, bool bRepaint);

        //[DllImport("user32.dll")]
        //[return: MarshalAs(UnmanagedType.Bool)]
        //private static extern bool GetWindowPlacement(IntPtr hWnd, ref WINDOWPLACEMENT lpwndpl);

        //[DllImport("user32.dll")]
        //private static extern bool SetWindowPlacement(IntPtr hWnd,
        //    WINDOWPLACEMENT lpwndpl);

        //[DllImport("user32.dll")]
        //[return: MarshalAs(UnmanagedType.Bool)]
        //private static extern bool EnableWindow(IntPtr hWnd, bool bEnable);

        private void getFanValuePatterns()
        {
            IUIAutomation mainAutomation = new CUIAutomationClass();
            if (Process.GetProcessesByName("speedfan").Length < 1)
            {
                Process.Start("SpeedFan.lnk");
            }
            else
            {
                foreach (Process p in Process.GetProcessesByName("speedfan"))
                {
                    IntPtr speedfanHandleTemp = (IntPtr) FindWindow(null, "SpeedFan 4.52");
                    ShowWindow(speedfanHandleTemp, ShowWindowEnum.Show);
                    ShowWindow(speedfanHandleTemp, ShowWindowEnum.Show);
                    IUIAutomationElement speedfanMainTemp = mainAutomation.ElementFromHandle(speedfanHandleTemp);
                    IUIAutomationWindowPattern windoo = speedfanMainTemp.GetCurrentPattern(10009) as IUIAutomationWindowPattern;
                    windoo?.Close();
                    p.Close();
                }
                Process.Start("SpeedFan.lnk");
            }

            IntPtr speedfanHandle = (IntPtr) FindWindow(null, "SpeedFan 4.52");
            while (speedfanHandle == IntPtr.Zero)
                speedfanHandle = (IntPtr) FindWindow(null, "SpeedFan 4.52");
            Rect speedFanRect = new Rect();

            //"capture" window and move it so we dont have to watch it load
            GetWindowRect(speedfanHandle, ref speedFanRect);
            //MoveWindow(speedfanHandle, -1000, -1000, speedFanRect.Right - speedFanRect.Left, speedFanRect.Bottom - speedFanRect.Top, false);
            //30012 = ClassName
            //10002 = ValuePattern
            bool alreadyNulled = false;
            NulledControl:
            //IUIAutomation is evil. but the following code is pretty self explanatory.
            IUIAutomationCondition condition_TPageControl = mainAutomation.CreatePropertyCondition(30012, "TPageControl");
            IUIAutomationCondition condition_TTabSheet = mainAutomation.CreatePropertyCondition(30012, "TTabSheet");
            IUIAutomationCondition condition_TRxSpinEdit = mainAutomation.CreatePropertyCondition(30012, "TRxSpinEdit");
            IUIAutomationCondition condition_TMemo = mainAutomation.CreatePropertyCondition(30012, "TMemo");

            IUIAutomationElement speedfanMain = mainAutomation.ElementFromHandle(speedfanHandle);
            IUIAutomationElement element_TPageControl = speedfanMain.FindFirst(TreeScope.TreeScope_Children, condition_TPageControl);
            IUIAutomationElement element_TTabSheet = element_TPageControl.FindFirst(TreeScope.TreeScope_Children, condition_TTabSheet);

            //waits for speedfan to fully initialize
            IUIAutomationElement element_TMemo = element_TTabSheet.FindFirst(TreeScope.TreeScope_Children, condition_TMemo);
            while ((element_TMemo.GetCurrentPattern(10002) as IUIAutomationValuePattern).CurrentValue.EndsWith(" Events"))
                Thread.Sleep(100);

            IUIAutomationCondition condition_TJvXPButton = mainAutomation.CreatePropertyCondition(30012, "TJvXPButton");
            IUIAutomationTreeWalker TjvWalker = mainAutomation.CreateTreeWalker(condition_TJvXPButton);
            IUIAutomationElement element_TJvXPButtonConfigure = element_TTabSheet.FindFirst(TreeScope.TreeScope_Children, condition_TJvXPButton);
            IUIAutomationElement element_TJvXPButtonMinimize = TjvWalker.GetNextSiblingElement(element_TJvXPButtonConfigure);

            //"primary control"
            IUIAutomationElement element_TRxSpinEdit = element_TTabSheet.FindFirst(TreeScope.TreeScope_Children, condition_TRxSpinEdit);
            //get all sibling Trx Controls
            List<IUIAutomationElement> TrxControls = new List<IUIAutomationElement> {element_TRxSpinEdit};
            IUIAutomationElement temporaryTrx = element_TRxSpinEdit;
            while (temporaryTrx != null)
            {
                IUIAutomationTreeWalker TrxWalker = mainAutomation.CreateTreeWalker(condition_TRxSpinEdit);
                temporaryTrx = TrxWalker.GetNextSiblingElement(TrxControls[TrxControls.Count - 1]);
                if (temporaryTrx != null)
                    TrxControls.Add(temporaryTrx);
            }
            //check for totally nulled controls list, means we are just shy of the program loading. wait 1 second and try again. if it fails then something broke
            if (TrxControls[0] == null)
            {
                if (alreadyNulled)
                    return;
                alreadyNulled = true;
                Thread.Sleep(1000);
                goto NulledControl;
            }
            //have to send twice, windows' fault.
            SendMessage(element_TJvXPButtonMinimize.CurrentNativeWindowHandle, 0x0201, IntPtr.Zero, IntPtr.Zero);
            SendMessage(element_TJvXPButtonMinimize.CurrentNativeWindowHandle, 0x0202, IntPtr.Zero, IntPtr.Zero);

            //store all patterns once for use later
            IUIAutomationValuePattern[] TrxPatterns = new IUIAutomationValuePattern[TrxControls.Count];
            for (int i = 0; i < TrxControls.Count; i++)
            {
                TrxPatterns[i] = TrxControls[i].GetCurrentPattern(10002) as IUIAutomationValuePattern;
            }

            //show window again even though its broken now
            //ShowWindow(speedfanHandle, ShowWindowEnum.Hide);
            //MoveWindow(speedfanHandle, speedFanRect.Left, speedFanRect.Top, speedFanRect.Right - speedFanRect.Left, speedFanRect.Bottom - speedFanRect.Top, false);

            fanValuePatterns = TrxPatterns;
        }


        //easier to read
        private enum ShowWindowEnum
        {
            Hide = 0,
            Show = 5
        }

        private struct Rect
        {
            public int Left { get; set; }
            public int Top { get; set; }
            public int Right { get; set; }
            public int Bottom { get; set; }
        }

        //[StructLayout(LayoutKind.Sequential)]
        //private struct WINDOWPLACEMENT
        //{
        //    public readonly int length;
        //    public readonly int flags;
        //    public readonly int showCmd;
        //    public readonly Point ptMinPosition;
        //    public readonly Point ptMaxPosition;
        //    public readonly Rectangle rcNormalPosition;
        //}


    }
}