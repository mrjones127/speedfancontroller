﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;

namespace AlchemyFanController2
{
    public class GraphControlPoint : UserControl
    {
        public CustomLine leftLine;
        public CustomLine rightLine;
        public double x;
        public double y;
        public GraphControlPoint(double x, double y)
        {
            Background = new SolidColorBrush(Colors.Red);
            Foreground = new SolidColorBrush(Colors.Red);
            Width = 8;
            Height = 8;
            this.x = x;
            this.y = y;
            Canvas.SetLeft(this, x);
            Canvas.SetRight(this, y);
        }
    }
}
